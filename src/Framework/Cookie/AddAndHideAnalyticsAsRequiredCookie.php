<?php declare(strict_types=1);

namespace Okeonline\VloerkleedCustom\Framework\Cookie;

use Shopware\Storefront\Framework\Cookie\CookieProviderInterface;

class AddAndHideAnalyticsAsRequiredCookie implements CookieProviderInterface {

    private $originalService;

    public function __construct(CookieProviderInterface $service)
    {
        // Decorate: Shopware\Storefront\Framework\Cookie\CookieProviderInterface
        // Decorate: Shopware\Storefront\Framework\Cookie\CookieProvider
        $this->originalService = $service;
    }

    public function getCookieGroups(): array
    {
        // Get original cookie-stack:
        $originalCookieGroups = $this->originalService->getCookieGroups();

        // Add a entry in the first (required) group:
        $originalCookieGroups[0]['entries'][] = [
            'snippet_name' => 'cookie.groupRequiredGoogleAnalytics',
            'cookie' => 'google-analytics-enabled',
            'expiration' => '30',
            'value' => '1',
            'hidden' => true,
        ];

        // return new cookie-stack
        return $originalCookieGroups;

    }
}
<?php declare(strict_types=1);

namespace Okeonline\VloerkleedCustom\Kernel\Subscriber;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class KernelRequestSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.request' => ['setGoogleAnalyticsCookie', 9990]
        ];
    }

    public function setGoogleAnalyticsCookie(RequestEvent $event)
    {
        if( ! $event->getRequest() instanceof Request )
            return;

        // If cookie not already exists:
        if( ! $event->getRequest()->cookies->has('google-analytics-enabled'))
        {
            // Set cookie
            // I can't see why symfony's cookie method wont work, but the native php setcookie does work...
            // Cookie::create('google-analytics-enabled', '1',  time() + (60*60*24*30));
            setcookie('google-analytics-enabled', '1',  time() + (60*60*24*30));
        }
    }
}

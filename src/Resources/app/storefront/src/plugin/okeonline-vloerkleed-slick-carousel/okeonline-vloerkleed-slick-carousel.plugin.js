import Plugin from 'src/plugin-system/plugin.class';
import slick from "@slick-carousel";
import jQuery from 'jquery/dist/jquery.slim'

export default class OkeonlineVloerkleedSlickCarouselPlugin extends Plugin {
    //Every slick function has to be inside init(); jQuery is available.
    init() {
        jQuery('.vk-custom-related-products-carousel').slick({
            dots: false,
            arrows: true,
            infinite: false,
            speed: 300,
            slidesToShow: 8,
            slidesToScroll: 1,
            prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
            nextArrow:"<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

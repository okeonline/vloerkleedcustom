/**
 * Note to developer
 *
 * To add a plugin:
 * 1. create it inside the /plugin directory,
 * 2. import and register/extend the plugin in this file afterwards.
 * 3. make sure that all permissions are set correctly (dockware-workspace will fix this on `docker-compose down/up`).
 * 4. add plugin as data-attribute to html-element inside twig
 *
 * e.g.:
 * docker-compose down
 * docker-compose up
 * docker-compose exec shopware bin/build-storefront.sh
 * docker-compose exec shopware bin/console cache:clear
 *
 */

/*Import plugins*/
import OkeonlineVloerkleedSlickCarouselPlugin           from "./plugin/okeonline-vloerkleed-slick-carousel/okeonline-vloerkleed-slick-carousel.plugin";

const PluginManager = window.PluginManager;

//Register
PluginManager.register('OkeonlineVloerkleedSlickCarouselPlugin', OkeonlineVloerkleedSlickCarouselPlugin, '[data-oo-vk-slick-carousel]');

console.log('test123');
